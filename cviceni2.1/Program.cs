﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._1
{
    class Program
    {
        class List
        {
            public int data;
            public List next;
        }

        class Fronta
        {
            public List start;
            public List end;
        }

        static Fronta Create()
        {
            Fronta F = new Fronta();
            F.end = null;
            F.start = null;
            return F;
        }

        static void Vypis(Fronta f)
        {
            while (f.start != f.end)
            {
                Console.Write("{0} ", f.start.data);
                f.start = f.start.next;
            }
            Console.Write("{0}", f.end.data);
        }

        static void Enqueue(Fronta f, int prvek)
        {
            if (f.end == f.start && f.end != null)
            {
                f.end = new List();
                f.end.data = prvek;
                f.start.next = f.end;
            }

            else if (f.end == null)
            {
                f.end = new List();
                f.end.data = prvek;
                f.start = f.end;
            }

            else
            {
                List tmp = new List();
                tmp.data = prvek;
                f.end.next = tmp;
                f.end = tmp;
            }
        }

        static int Dequeue(Fronta f)
        {
            List tmp = new List();

            if (f.start == null)
                Console.WriteLine("Fronta je prazdna.");

            int prvek = f.start.data;
            tmp = f.start.next;
            f.start = tmp;

            if (f.start == null)
                f.end = null;
            return prvek;
        }

        static int Front(Fronta f)
        {
            if (f.start == null)
                Console.WriteLine("Fronta je prazdna.");
            return f.start.data;
        }

        static bool IsEmpty(Fronta f)
        {
            if (f.start == null)
                return true;
            return false;
        }

        static void Main(string[] args)
        {
            Fronta f = Create();
            Enqueue(f, 4);
            Enqueue(f, 1);
            Enqueue(f, 8);
            int i1 = Dequeue(f); // 4 
            int i2 = Dequeue(f);  // 1
            int i3 = Front(f); // 8
            bool b = IsEmpty(f); // false
            Enqueue(f, 5);
            Enqueue(f, 7);
            Vypis(f); // 8 5 7
            Console.Read();
        }
    }
}
